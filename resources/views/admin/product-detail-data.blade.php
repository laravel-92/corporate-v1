@extends('admin.index')
@section('main-content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Detail Data Product</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                    <li class="breadcrumb-item active">Detail Data Product</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="product-container">
    <div class="row">
        <div class="col-4">
            <img src="{{asset('storage/'.$data->productImg)}}" class="img-fluid float-start mr-2">
        </div>

        <div class="col-8">
            <div class="text-center">
                <h2>{{$data->title}}</h2>
            </div>

            {!!$data->description!!}

            <div class="mt-5">
                <a href="{{url('admin/product/'.$data->id.'/edit-data')}}" class="btn btn-warning waves-effect waves-light btn-lg m-1"><i class="feather-edit-3"></i> Edit Data Product</a>

                <form action="{{url('admin/'.$data->id.'/delete')}}" method="POST" class="d-inline" id="deleteForm-{{$data->id}}">
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger waves-effect waves-light btn-lg m-1 deleteButton" data-id="{{$data->id}}"><i class="feather-trash-2"></i> Delete Data Product</button>
                </form>
                <a href="{{url('admin/product')}}" class="btn btn-danger waves-effect waves-light btn-lg m-1"><i class="feather-arrow-left"></i> Back to product</a>
            </div>
        </div>
    </div>
</div>
@endsection