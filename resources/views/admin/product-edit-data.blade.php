@extends('admin.index')
@section('main-content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Edit Data Product</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                    <li class="breadcrumb-item active">Edit Data Product</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

            {{-- FORM START --}}
            <form action="{{url('admin/product/{id}/edit-data')}}" method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="id" value="{{$data->id}}">

                <div class="form-group">
                    <h4 class="card-title">Product Title</h4>
                    <input type="text" name="title" class="form-control" value="{{ $data->title }}" autofocus>
                </div>

                <h4 class="card-title">Description</h4>
                <p class="card-subtitle mb-4">Write your description product detail for example write grade your product, min order and more detail product</p>

                <div id="snow-editor" style="height: 300px;">
                    {!! $data->description !!}
                </div> <!-- end Snow-editor-->
                <input type="hidden" name="description" id="quill-content">

                <div class="pt-3">
                    <h4 class="card-title">Product Upload</h4>
                    <p class="card-subtitle mb-4">Upload your product IMG max 1 IMG</p>
                                    
                    <input type="file" name="productImg" class="dropify" data-height="125" />
                </div>

                <div class="form-group pt-3">
                    <button type="submit" name="save" class="btn btn-success waves-effect waves-light"><i class="feather-save"></i> Update Data</button>
                    <a href="{{url('admin/product')}}" class="btn btn-danger waves-effect waves-light"><i class="feather-arrow-left"></i> Back to product</a>
                </div>

            </form> <!-- end form-->
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col -->
</div>
<!-- end row-->

<script>
    // Setel nilai input tersembunyi dengan konten Quill sebelum pengiriman formulir
    document.querySelector('form').addEventListener('submit', function (e) {
        e.preventDefault();
        document.getElementById('quill-content').value = quill.root.innerHTML;
        this.submit(); // Kirim formulir
    });
</script>
@endsection