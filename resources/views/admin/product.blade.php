@extends('admin.index')
@section('main-content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Product</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                    <li class="breadcrumb-item active">Product</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

@if (session()->has('success'))
    <script>
        Swal.fire("Success!", "{{session('success')}}", "success");
    </script>
@elseif (session()->has('update'))
    <script>
        Swal.fire("Success!", "{{session('update')}}", "success");
    </script>
@elseif (session()->has('delete'))    
    <script>
        Swal.fire("Deleted!", "{{session('delete')}}", "success");
    </script>
@endif

{{-- row main content --}}
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Table List</h4>
                <p class="card-subtitle mb-4">
                </p>

                <a href="{{url('admin/product/add-data')}}" class="btn btn-primary waves-effect waves-light btn-sm mb-3"><i class="feather-file-plus"></i> Add Data</a>

                <table id="basic-datatable" class="table table-striped dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                                
                                
                    <tbody>
                        @foreach ($data as $index => $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <a href="{{url('admin/product/'.$item->id.'/edit-data')}}" data-toggle="tooltip" data-placement="bottom" title="Edit Data" class="btn btn-warning waves-effect waves-light"><i class="feather-edit-3"></i></a>

                                    <a href="{{url('admin/'.$item->id.'/detail')}}" data-toggle="tooltip" data-placement="bottom" title="Detail Data" class="btn btn-secondary waves-effect waves-light"><i class="feather-file-text"></i></a>

                                    <form action="{{url('admin/'.$item->id.'/delete')}}" method="POST" class="d-inline" id="deleteForm-{{$item->id}}">
                                        @method('delete')
                                        @csrf
                                        <button data-toggle="tooltip" data-placement="bottom" title="Delete Data" class="btn btn-danger waves-effect waves-light deleteButton" data-id="{{$item->id}}"><i class="feather-trash-2"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                            
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
{{-- end row main content --}}

@endsection