@extends('admin.index')
@section('main-content')
<div class="row">
    <div class="col-12">
        <!-- start page title -->
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Banner Setting</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                    <li class="breadcrumb-item active">Setting</li>
                    <li class="breadcrumb-item active">Banner</li>
                </ol>
            </div>

        </div>
        <!-- end page title -->
    </div>
</div>

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">

            {{-- FORM START --}}
            <form action="{{url('')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <h4 class="card-title">Banner Title 1</h4>
                    <input type="text" name="title1" class="form-control" placeholder="Enter your banner title" autofocus>
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 2</h4>
                    <input type="text" name="title2" class="form-control" placeholder="Enter your banner title" autofocus>
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner Image Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 1900 x 550 px</strong></p>
                                    
                    <input type="file" name="banner[0]" class="dropify" data-height="125" />
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 3</h4>
                    <input type="text" name="title3" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 4</h4>
                    <input type="text" name="title4" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner Image Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 750 x 748 px</strong></p>
                                    
                    <input type="file" name="banner[1]" class="dropify" data-height="125" />
                </div>
                <div class="form-group">
                    <h4 class="card-title">Banner Title 5</h4>
                    <input type="text" name="title5" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 6</h4>
                    <input type="text" name="title6" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner Image Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 750 x 748 px</strong></p>
                                    
                    <input type="file" name="banner[2]" class="dropify" data-height="125" />
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 7</h4>
                    <input type="text" name="title7" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 8</h4>
                    <input type="text" name="title8" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner Image Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 750 x 748 px</strong></p>
                                    
                    <input type="file" name="banner[3]" class="dropify" data-height="125" />
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 9</h4>
                    <input type="text" name="title9" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 10</h4>
                    <input type="text" name="title10" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 11</h4>
                    <input type="text" name="title11" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 12</h4>
                    <input type="text" name="title12" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 13</h4>
                    <input type="text" name="title13" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 14</h4>
                    <input type="text" name="title14" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 15</h4>
                    <input type="text" name="title15" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner video Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 750 x 421 px</strong></p>
                                    
                    <input type="file" name="banner[4]" class="dropify" data-height="125" />
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 16</h4>
                    <input type="text" name="title16" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="form-group">
                    <h4 class="card-title">Banner Title 17</h4>
                    <input type="text" name="title17" class="form-control" placeholder="Enter your banner title">
                </div>

                <div class="pt-3">
                    <h4 class="card-title">Banner video Upload</h4>
                    <p class="card-subtitle mb-4" style="color : #ff0000"><strong>Upload banner with size 341 x 557 px</strong></p>
                                    
                    <input type="file" name="banner[5]" class="dropify" data-height="125" />
                </div>

                <div class="form-group pt-3">
                    <button type="submit" name="save" class="btn btn-success waves-effect waves-light"><i class="feather-save"></i> Save Data</button>
                    <a href="{{url('admin/product')}}" class="btn btn-danger waves-effect waves-light"><i class="feather-arrow-left"></i> Back to product</a>
                </div>

            </form> <!-- end form-->
            </div> <!-- end card-body-->
        </div> <!-- end card-->

    </div> <!-- end col -->
</div> <!-- end row -->
@endsection