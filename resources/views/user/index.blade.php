<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en"><!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Continal</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Font
  ================================================== -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}"/>
	<link rel="stylesheet" href="{{asset('rs-plugin/css/settings.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/rev-settings.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('style.css')}}"/>
	
	<!-- Favicons
	================================================== -->
	
</head>
<body class="homepage">	
	<div class="top-line"></div>
	<header class="static header2">		
		<div class="container">
			<h1 class="logo">
				<a href="index.html"><img src="{{asset('images/logo.png')}}" alt=""></a>
			</h1>						
			<div class="top-info">
				<p><span>Free Call:</span> (+1)-96-716-6879</p>
				<p class="e-mail"><span>Email:</span> <a href="#">contact@site.com</a></p>		
			</div>
			<button class="btn-toggle"><i class="fa fa-reorder"></i></button>
			<nav class="nav">				
				<ul class="main-menu">
					<li><a href="{{url('/')}}">Home</a></li>
					<li><a href="{{url('product')}}">Product</a></li>					
					<li><a href="{{url('about')}}">About Us</a></li>					
					<li><a href="{{url('contact')}}">Contact Us</a></li>					
				</ul>
			</nav>
		</div>
	</header>

	<div id="content">
		<div class="entry-content">

			@yield('main-content')

		</div>		
	</div>

	<footer>
		<div class="widget-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="widget">
							<h4><img src="images/logo-footer.png" alt=""></h4>
							<p>Continal is a clean PSD theme suitable for corporate, construction. You can customize it very easy to fit your needs, semper suscipit metus accumsan at. Vestibulum et lacus urna. </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<h4>Contact Us</h4>
						<div class="textwidget">
							<p><i class="fa fa-home"></i> 379 5th Ave  New York, NYC 10018</p>
							<p><i class="fa fa-phone"></i> (+1) 96 716 6879</p>
							<p><i class="fa fa-fax"></i> (+1) 96 716 6879</p>
							<p><i class="fa fa-envelope-o"></i> contact@site.com</p>
							<p><i class="fa fa-clock-o"></i> Mon-Fri 09:00 - 17:00 Mon-Fri</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="widget">
							<h4>Company</h4>
							<div class="row">
								<div class="col-xs-6 col-lg-5">
									<ul>
										<li><a href="#">Home</a></li>
										<li><a href="#">Projects</a></li>
										<li><a href="#">Services</a></li>
										<li><a href="#">About Us</a></li>
										<li><a href="#">Contact</a></li>
									</ul>
								</div>
								<div class="col-xs-6 col-lg-7">
									<ul>
										<li><a href="#">Blog</a></li>
										<li><a href="#">404 Page</a></li>
										<li><a href="#">Shop</a></li>
										<li><a href="#">Elements</a></li>
										<li><a href="#">Typography</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						{{-- <h4>Gallery</h4>
						<div class="gallery-image">
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
							<a href="#"><img src="http://placehold.it/80x80/ccc.jpg" alt=""></a>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="sub-footer">
			<div class="container">
				<p>Copyright © 2016 Designed by AuThemes. All rights reserved.</p>
			</div>
		</div>
	</footer>
	<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>	
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/classie.js')}}"></script>
<script type="text/javascript"> 
(function($) { "use strict";
	$('.vimeo a,.youtube a').click(function (e) {
		e.preventDefault();
		var videoLink = $(this).attr('href');
		var classeV = $(this).parent();
		var PlaceV = $(this).parent();
		if ($(this).parent().hasClass('youtube')) {
			$(this).parent().wrapAll('<div class="video-wrapper">');
			$(PlaceV).html('<iframe frameborder="0" height="333" src="' + videoLink + '?autoplay=1&showinfo=0" title="YouTube video player" width="100%"></iframe>');
		} else {
			$(this).parent().wrapAll('<div class="video-wrapper">');
			$(PlaceV).html('<iframe src="' + videoLink + '?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;color=cfa144" width="100%" height="300" frameborder="0"></iframe>');
		}
	});		
})(jQuery);
</script>
<script type="text/javascript" src="{{asset('js/custom-index.js')}}"></script>

<!-- SLIDER REVOLUTION SCRIPTS  -->
<script type="text/javascript" src="{{asset('rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<script type="text/javascript" src="{{asset('rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('js/revslider-custom.js')}}"></script>
</body>
</html>