@extends('user.index')
@section('main-content')

<div class="page-title">
    <div class="container">
        <h1>Products</h1>
    </div>
</div>

<div class="breadcrumbs">
    <div class="container">
        <ul class="crumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li> <span>/</span>
            <li class="active"> Product</li>
        </ul>
    </div>
</div>
<div class="page-content">
    <section class="list-projects">
        <div class="container">
            {{-- <div class="row">
                <div class="col-md-12">
                    <ul id="filter" class="filter-projects none-style">
                        <li><a href="#" class="current" data-filter="*" title="">All Projects</a></li>
                        <li><a href="#" data-filter=".interior" title="">Interior Design </a></li>
                        <li><a href="#" data-filter=".education" title="">Education</a></li>
                        <li><a href="#" data-filter=".office" title="">Office</a></li>
                        <li><a href="#" data-filter=".health" title="">Health</a></li>
                        <li><a href="#" data-filter=".house" title="">House</a></li>
                        <li><a href="#" data-filter=".park" title="">Green Park</a></li>
                    </ul>
                </div>							
            </div> --}}

            
            {{-- MAIN PRODUCT --}}
            <div class="row">
                <div id="gallery" class="all-project">
                    @foreach ($data as $item)
                        <div class="col-md-4 col-sm-6 item office">									
                            <div class="project-box ">
                                <a href="#" class="image-project">
                                    <img src="{{asset('storage/'.$item->productImg)}}" alt="">
                                    <span class="overlay"></span>
                                </a>
                                <h4><a href="#">{{ $item->title }}</a></h4>
                                {{-- <div class="cat-name"><a href="#">Office</a>, <a href="#">Education</a></div> --}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{-- END MAIN PRODUCT --}}

        </div>
    </section>

    <section class="action-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-action">
                        <h4>We’ve Completed More Than 100+ project for our amazing clients, if you interested?</h4>
                        <a href="contact.html" class="ot-btn btn-color">Get a quote</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
                
</div>

@endsection