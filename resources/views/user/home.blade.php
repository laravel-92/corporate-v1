@extends('user.index')
@section('main-content')

<div class="page-content">

    <section class="home-slider">
        <div id="slider">
            <!-- revolution slider begin -->
            <div class="fullwidthbanner-container">
                <div id="revolution-slider-2">

                    <ul>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1000">
                            <!--  BACKGROUND IMAGE -->
                            <img src="http://placehold.it/1900x550/ccc.jpg" alt="">

                            <div class="tp-caption sft custom-font-2 tp-resizeme"
                                data-x="center"
                                data-y="188"
                                data-speed="400"
                                data-start="1000"
                                data-easing="easeInOut">
                                Welcome to <span class="color">Continal</span>
                            </div>

                            <div class="tp-caption sft stb custom-font-1 tp-resizeme"
                                data-x="center"
                                data-y="230"
                                data-speed="400"
                                data-start="1400"
                                data-easing="easeInOut">
                                Construction company
                            </div>
                          
                            <div class="tp-caption sfb tp-resizeme"
                                data-x="center"
                                data-y="320"
                                data-speed="400"
                                data-start="1800"
                                data-easing="easeInOut">
                                <a class="ot-btn btn-border" href="contact.html">Get a quote</a>
                            </div>
                        </li>

                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-delay="6000">
                            <!--  BACKGROUND IMAGE -->
                            <img src="http://placehold.it/1900x550/ccc.jpg" alt="">

                            <div class="tp-caption sft custom-font-2 tp-resizeme"
                                data-x="center"
                                data-y="188"
                                data-speed="400"
                                data-start="1000"
                                data-easing="easeInOut">
                                Customer Focused
                            </div>

                            <div class="tp-caption sft custom-font-1 tp-resizeme"
                                data-x="center"
                                data-y="230"
                                data-speed="400"
                                data-start="1400"
                                data-easing="easeInOut">
                                Construction Solutions
                            </div>
                            
                            <div class="tp-caption sfb tp-resizeme"
                                data-x="center"
                                data-y="320"
                                data-speed="400"
                                data-start="1700"
                                data-easing="easeInOut">
                                <a class="ot-btn btn-border tp-resizeme" href="contact.html">Get a quote</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- revolution slider close -->
        </div>
    </section>

    <section class="features-home2">
        <div class="container">
            <div class="list-features">

                <div class="col-md-4">
                    <div class="features-box">
                        <img src="http://placehold.it/750x748/ccc.jpg" alt="">
                        <div class="overlay">
                            <div class="content-mid">
                                <div class="inner">
                                    <h4>QUALITY COMES FIRST</h4>
                                    <p>Quisque pulvinar libero dolor, quis bibendum eros euismod sit amet.</p>
                                    <a href="#" class="more-link">Read More</a>
                                </div>
                            </div>
                        </div>										
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="features-box">
                        <img src="http://placehold.it/750x748/ccc.jpg" alt="">
                        <div class="overlay">
                            <div class="content-mid">
                                <div class="inner">
                                    <h4>Construction Managment</h4>
                                    <p>Diam euismod metus vehicula varius. Donec et velit placerat arcu lobortis.</p>
                                    <a href="#" class="more-link">Read More</a>
                                </div>
                            </div>
                        </div>										
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="features-box">
                        <img src="http://placehold.it/750x748/ccc.jpg" alt="">
                        <div class="overlay">
                            <div class="content-mid">
                                <div class="inner">
                                    <h4>HIGH TECHNOLOGIES</h4>
                                    <p>Convallis a, tempor sed magna. Pellentesque non diam euismod metus vehicula</p>
                                    <a href="#" class="more-link">Read More</a>
                                </div>
                            </div>
                        </div>										
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="shadow-section">
        <div class="container">
            <div class="box-shadow"></div>
        </div>
    </section>

    <section class="why-choose">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center after">WHY CHOOSE CONTINAL</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <figure class="vimeo"> 
                        <a href="http://player.vimeo.com/video/112734492">
                            <img src="http://placehold.it/750x421/ccc.jpg" alt="" />
                            <img class="btn-play" src="images/video-play.png" alt="" />
                        </a>
                    </figure>
                </div>
                <div class="col-md-6">
                    <div class="features box4">
                        <h4><i class="fa fa-cogs"></i>Transparency</h4>
                        <p>Morbi vehicula a nibh in commodo. Aliquam quis dolor eget lectus pulvinar malesuada. Suspendisse eu rhoncus ligula. </p>
                    </div>
                    <div class="features box4">
                        <h4><span><i class="fa fa-diamond"></i>Expertise</span></h4>
                        <p>Nam orci metus, varius at nisl at, tempor facilisis magna. Ut maximus felis tincidunt lacinia. Nulla malesuada ipsum at magna condimentum pharetra.</p>
                    </div>
                    <div class="features box4">
                        <h4><span><i class="fa fa-suitcase"></i>Reliability</span></h4>
                        <p>Fusce viverra risus diam, in luctus nulla porta vel. Etiam nunc lorem, dapibus augue vitae, lacinia pharetra eros. Fusce ac egestas purus, non porta est.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="shadow-section">
        <div class="container">
            <div class="box-shadow"></div>
        </div>
    </section>

    <section class="action-image features-about" style="padding-bottom: 1em">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="get-action dark">
                        <h4>WORKING WITH US</h4>
                        <p>WE DESIGN, CONSTRUCT, REFURBISH AND OPERATE OUTSTANDING BUILDINGS.</p>
                        <div><a href="#" class="ot-btn btn-color btn-small">Get a Quote</a></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-right">
                        <img src="http://placehold.it/341x557/ccc.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection