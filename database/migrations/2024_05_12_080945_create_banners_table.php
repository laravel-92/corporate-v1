<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id()->unsignedTinyInteger();
            $table->string('title1');
            $table->string('title2');
            $table->string('banner1');
            $table->string('title3');
            $table->string('title4');
            $table->string('banner2');
            $table->string('title5');
            $table->string('title6');
            $table->string('banner3');
            $table->string('title7');
            $table->string('title8');
            $table->string('banner4');
            $table->string('title9');
            $table->string('title10');
            $table->string('title11');
            $table->string('title12');
            $table->string('title13');
            $table->string('title14');
            $table->string('title15');
            $table->string('banner5');
            $table->string('title16');
            $table->string('title17');
            $table->string('banner6');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
};
