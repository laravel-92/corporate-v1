<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserpageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// USER
// HOME
Route::get('/', [UserpageController::class, 'home']);

// PRODUCT
Route::get('product', [UserpageController::class, 'productList']);



// ADMIN
// DASHBOARD
Route::get('/admin', [AdminController::class, 'dashboard']);

// PRODUCT
Route::get('/admin/product', [ProductController::class, 'index']);
Route::get('/admin/product/add-data', [ProductController::class, 'addData']);
Route::post('/admin/product/add-data', [ProductController::class, 'insertData']);
Route::get('/admin/{id}/detail', [ProductController::class, 'detailData']);
Route::get('/admin/product/{id}/edit-data', [ProductController::class, 'editData']);
Route::post('/admin/product/{id}/edit-data', [ProductController::class, 'updateData']);
Route::delete('/admin/{id}/delete', [ProductController::class, 'dropData']);

// MAIN BANNER SETTING
Route::get('/admin/main-user-banner', [AdminController::class, 'mainUser_banner']);