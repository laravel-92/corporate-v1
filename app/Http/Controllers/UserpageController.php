<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class UserpageController extends Controller
{
    public function home()
    {
        return view('user.home');
    }

    public function productList()
    {
        $data = Product::all();
        return view('user.product', ['data' => $data]);
    }
}
