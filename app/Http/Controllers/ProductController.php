<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return view('admin.product', ['data' => $data]);
    }

    public function addData()
    {
        return view('admin.product-add-data');
    }

    public function insertData(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'productImg' => 'required|image'
        ]);

        $data['productImg'] = $request->file('productImg')->store('product-image');

        $insert = Product::create($data);

        return redirect(url('admin/product'))->with('success', 'Data has been added');
    }

    public function detailData(Request $request)
    {
        $data = Product::find($request->id);
        return view('admin.product-detail-data', ['data' => $data]);
    }

    public function editData(Request $request)
    {
        $data = Product::find($request->id);
        return view('admin.product-edit-data', ['data' => $data]);
    }

    public function updateData(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'productImg' => 'image'
        ]);

        if($request->productImg){
            $path = Product::where('id', $request->id)->get();
            Storage::delete($path[0]['productImg']);

            $data['productImg'] = $request->file('productImg')->store('product-image');

            $update = Product::where('id', $request->id)->update($data);
            return redirect(url('admin/product'))->with('update', 'Data has been updated');
        }else{
            $update = Product::where('id', $request->id)->update([
                'title' => $request['title'],
                'description' => $request['description']
            ]);
            return redirect(url('admin/product'))->with('update', 'Data has been updated');
        }
    }

    public function dropData(Request $request)
    {
        $path = Product::where('id', $request->id)->get();

        if(Storage::exists($path[0]['productImg'])){
            Storage::delete($path[0]['productImg']);
        }
        
        Product::destroy($request->id);
        return redirect(url('admin/product'))->with('delete', 'Your file has been deleted.');
    }
}
